WHEN TO USE CASSANDRA INSTEAD OF MONGO - 
Cassandra offers a solution for problems where one of your requirements is to have a very heavy write system and you want to have a quite responsive reporting system on top of that stored data. Consider the use case of Web analytics where log data is stored for each request and you want to built an analytical platform around it to count hits per hour, by browser, by IP, etc in a real time manner. [mongo vs cassandra](https://content-static.upwork.com/blog/uploads/sites/3/2016/06/16095633/Mongo-v-Cassandra-infographic1.png)

**MongoDB is good if your app requires…**

An RDBMS replacement, real-time analytics, high-speed logging, caching, and high scalability. However, it’s not as good if your app requires a highly transactional system (although it is built to handle transactions on single documents, so may be enough for your e-commerce needs), or traditional database requirements such as foreign key constraints.

---

- Mysql :- Mysql is open source rdbms based database being used for developing various web-based software applications.

Advantages:- 1. Scalability and Flexibility 2. High Performance 3. High Availability 4. Robust Transactional Support 5. Lowest Total Cost of Ownership

Disadvantages Of mysql :- 1. MySQL does not support a very large database size as efficiently. 2. Transactions are not handled very efficiently. 3. The development is not community driven so it has lagged behind. 4. The functionality tends to be heavily dependent on the addons.

- Oracle :- Oracle database (Oracle DB) is a relational database management system (RDBMS) from the Oracle Corporation. The system is built around a relational database framework in which data objects may be directly accessed by users (or an application front end) through structured query language (SQL)Oracle is a fully scalable relational database architecture and is often used by global enterprises, which manage and process data across wide and local area networks. The Oracle database has its own network component to allow communications across networks.

Advantages Of Oracle :- Oracle Database is cross-platform. It can run on various hardware across operating systems including Windows Server, Unix, and various distributions of GNU/Linux. Oracle Database has its networking stack that allows application from a different platform to communicate with the Oracle Database smoothly. For example, applications running on Windows can connect to the Oracle Database running on Unix. * ACID-compliant – Oracle is ACID-compliant Database that helps maintain data integrity and reliability.

Disadvantages:- One major disadvantage of Oracle database is its complexity. Using Oracle is not ideal if the users lack the technical ability and know-how needed to work with Oracle databases. It is also not ideal to use Oracle if an organization or individual is looking for an easy-to-use database with basic features.

- MS Sql Server The Microsoft relational database management system is a software product which primarily stores and retrieves data requested by other applications. These applications may run on the same or a different computer.

Advantages :- No coding needed Well defined standards Portability Interactive Language * Multiple data views

Disadvantages :- Difficult Interface Partial Control * High cost

- PostgreSql PostgreSQL is a powerful, open source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads.In addition to being free and open source, PostgreSQL is highly extensible. For example, you can define your own data types, build out custom functions, even write code from different programming languages without recompiling your database.

Advantages :- Easy to use. Has user-defined data type. Open source. A lot of community support. Make use of Stored procedures. It supports ACID i.e. Atomicity, Consistency, Isolation, Durability.

Disadvantages :- If we see the architecture of Postgre (Structured query language). in the above diagram, this creates separate service for every client. Which turns into a lot of memory utilization. If we do comparison PostgreSQL is not good when it comes to performance. It is not much popular than other database management systems. This also has a lack of skilled professionals. When it comes to speed PostgreSQL is not worthy as compared to other tools. Making replication is more complex. * Installation is not easy for the beginner.

- MongoDB MongoDB is an open source database management system (DBMS) that uses a document-oriented database model which supports various forms of data. It is one of numerous nonrelational database technologies which arose in the mid-2000s under the NoSQL banner for use in big data applications and other processing jobs involving data that doesn't fit well in a rigid relational model. Instead of using tables and rows as in relational databases, the MongoDB architecture is made up of collections and documents.

Advantages of mongodb:-

    Flexible Database
    Sharding
    High Speed
    Scalability

Disadvantages Of mongodb:- Joins not Supported High Memory Usage Limited Data Size Limited Nesting