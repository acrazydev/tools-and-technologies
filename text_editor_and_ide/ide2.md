
### *Text Editors -*
    Sublime Text
    Atom
    Notepad++
    CoffeeCup – The HTML Editor
    TextMate
    Vim
    UltraEdit
    Coda
    BBEdit
    Komodo Edit
    Visual Studio Code
    Brackets
    CodeShare


### *IDEs -*

1. Microsoft Visual Studio
#


Microsoft Visual Studio is a premium IDE ranging in price from $699 - $2,900 depending on the edition and licensing. The many editions of this IDE are capable of creating all types of programs ranging from web applications to mobile apps to video games. This series of software includes tons of tools for compatibility testing so that you can see how your apps run on more than 300 devices and browsers. Thanks to its flexibility, Visual Studio is a great tool for both students and professionals.

Languages Supported: ASP.NET, DHTML, JavaScript, JScript, Visual Basic, Visual C#, Visual C++, Visual F#, XAML and more

Notable Features:

    A massive library of extensions that is always growing
    IntelliSense
    Customizable dashboard and dockable windows
    Straightforward workflow and file hierarchy
    Insights for monitoring performance in real time
    Automation tools
    Easy refactoring and code snippet insertion
    Split screen support
    Error list that allows debugging while building
    Approval checks when deploying apps via ClickOnce, Windows Installer or Publish Wizard

Drawbacks: Because the Visual Studio is a heavyweight IDE, it takes considerable resources to open and run, so making simple edits may be time consuming on some devices. For simpler task, it may be easier to use a lightweight editor.
1. NetBeans
#

Netbeans is a free and open source IDE. Ideal for editing existing projects or starting from scratch, NetBeans boasts a simple drag-and-drop interface that comes with a myriad of convenient project templates. It is primarily used to develop Java applications, but you can download bundles that support other languages.

Languages Supported: C, C++, C++11, Fortan, HTML 5, Java, PHP and more

Notable Features:

    Intuitive drag-and-drop interface
    Dynamic and static libraries
    Multi-session GNU debugger integration with code assistance
    Allows for remote development
    Compatible with Windows, Linux, OS X, and Solaris platforms
    Supports Qt Toolkit
    Supports Fortan and Assembler files
    Supports a number of compilers including CLang/LLVM, Cygwin, GNU, MinGW and Oracle Solaris Studio

Drawbacks: This free IDE consumes a lot of memory, so it may perform sluggishly on some machines.
3. PyCharm
#

PyCharm is developed by the folks over at Jet Brains and provides users a free Community Edition, 30 day free trial for the Professional Edition, $213 - $690 for an annual subscription. Comprehensive code assistance and analysis make PyCharm the best IDE for Python programmers of all ability levels. PyCharm also supports other languages and works on multiple platforms, so practically anyone can use it.

Languages Supported: AngularJS, Coffee Script, CSS, Cython, HTML, JavaScript, Node.js, Python, TypeScript and template languages

Notable Features:

    Compatible with Windows, Linux, and Mac OS
    Comes with Django IDE
    Easy to integrate with Git, Mercurial and SVN
    Customizable interface with VIM emulation
    JavaScript, Python and Django debuggers
    Supports Google App Engine

Drawbacks: Users complain that PyCharm has some bugs, such as the autocomplete feature occasionally not working, which can be a minor inconvenience.
4. IntelliJ IDEA
#

IntelliJ IDEA is another IDE developed by Jet Brains. This IDE offers users a free Community Edition, 30 day free trial for the Ultimate Edition, and costs $533 - $693 annually depending on features. IntelliJ IDEA, which supports Java 8 and Java EE 7, comes with extensive tools to develop mobile apps and enterprise technologies for different platforms. When it comes to cost, IntelliJ is a real deal due to the massive of list of features you get.

Languages Supported: AngularJS, CoffeeScript, CS, HTML, JavaScript, Less, Node JS, PHP, Python, Ruby, Sass, TypeScript and more.

Notable Features:

    Extensive database editor and UML designer
    Supports multiple build systems
    Test runner UI
    Code coverage
    Git integration
    Supports Google App Engine, Grails, GWT, Hibernate, Java EE, OSGi, Play, Spring, Struts and more
    Deployment and debugging tools for most application servers
    Intelligent text editors for HTML, CSS, and Java
    Integrated version control
    AIR Mobile supports Android and iOS devices

Drawbacks: This IDE comes with a learning curve, so it may not the best for beginners. There are many shortcuts to remember, and some users complain about the clunky UI.
5. Eclipse
#

Eclipse is a free and flexible open source editor useful for beginners and pros alike. Originally a Java environment, Eclipse now has a wide range of capabilities thanks to a large number of plug-ins and extensions. In addition to debugging tools and Git/CVS support, the standard edition of Eclipse comes with Java and Plugin Development Tooling. If that’s not enough for you, there’s plenty of other packages to choose from that include tools for charting, modeling, reporting, testing and building GUIs. The Eclipse Marketplace Client gives users access to a treasure trove of plugins and information supplied by an expanding community of developers.

Languages Supported: C, C++, Java, Perl, PHP, Python, Ruby and more

Notable Features:

    A plethora of package solutions allowing for multi-language support
    Java IDE enhancements such as hierarchical views of nested projects with customizable perspectives
    Task-focused interface including system-tray notifications
    Automated error reporting
    Tooling options for JEE projects
    JUnit integration

Drawbacks: While Eclipse is very versatile software, the many options may be intimidating to newcomers. Eclipse doesn’t have all of the same features as IntelliJ IDEA, but it is open source.
6. Code::Blocks
#

Code::Blocks is another popular free and open source option. It is a highly customizable IDE that performs consistently across all platforms, so it is great for developers who frequently switch between workspaces. The plug-in framework lets users tweak this IDE to meet their needs.

Languages Supported: C, C++, Fortran

Notable Features:

    Easy-to-navigate tabbed interface including a list of open files
    Compatible with Linux, Mac, and Windows
    Written in C++
    Requires no interpreted or proprietary languages
    Supports many pre-built and custom-built plug-ins
    Supports multiple compilers including GCC, MSVC++, clang and more
    Debugger that includes breakpoint support
    Text editor with syntax highlighting and autocomplete
    Customizable external tools
    Simple task management tools ideal for multiple users

Drawbacks: Though Code::Blocks comes with many features, it is a relatively lightweight IDE, so it’s not suited for larger projects. It is a great tool for beginners, but advanced coders may be frustrated with the limitations.
7. Aptana Studio 3
#

Perhaps the most powerful of the open source IDEs, Aptana Studio 3 is a massive improvement over its predecessors. Since Aptana Studio 3 supports most browser specs, compatibility challenges are minimal, so users can quickly develop, test and deploy web apps from this single IDE.

Languages Supported: HTML5, CSS3, JavaScript, Ruby, Rails, PHP, and Python

Notable Features:

    Code assist for CSS, HTML, JavaScript, PHP and Ruby
    Deployment wizard with simple setup and multiple protocols including Capistrano, FTP, FTPS and SFTP
    Automatically sends Ruby and Rails applications to hosting services
    Integrated debuggers for Ruby and Rails and JavaScript
    Git integration
    Easily accessible command line terminal with hundreds of commands
    String custom commands to extend capabilities

Drawbacks: Although Aptana works well for students juggling multiple small projects, it has stability issues and runs slowly, so professional developers might prefer a more powerful IDE.
8. Komodo
#

Komodo offers a free 21-day trial and costs between $99 - $1615 depending on the edition and licensing. Practically any programmer can use Komodo because it supports most major programming languages. The streamlined interface allows for advanced editing, and small perks like the Syntax Checker and single-step debugging make Komodo one of the most popular IDEs for web and mobile development.

Languages Supported: CSS, Go, JavaScript, HTML, NodeJS, PerlPHP, Python, Ruby, Tcl and more.

Notable Features:

    Customizable UI including split view and multi-window editing
    Version control integration for Bazaar, CVS, Git, Mercurial, Perforce and Subversion
    Python and PHP code profiling
    Convenient code collaboration for multi-user editing
    Deploy to the cloud thanks to Stackato PaaS
    Graphical debugging for NodeJS, Perl, PHP, Python, Ruby and Tcl
    Autocomplete and refactoring
    Consistent performance across Mac, Linux and Windows platforms
    Many add-ons allow a high level of customization

Drawbacks: One of the few complaints about Komodo is that the free version doesn’t enable all of the features, but the premium version is still considered well-worth the cost.
9. RubyMine
#

RubyMine is another premium IDE, developed by Jet Brains, that offers a 30 day free trial and costs $210 - 687 annually. As its name implies, RubyMine is a favorite among Ruby enthusiasts; however, this IDE supports other programming languages as well. Easy navigation, logical workflow organization, and compatibility with most platforms make RubyMine a workplace favorite.

Languages Supported: CoffeeScript, CSS, HAML, HTML, JavaScript, Less, Ruby and Rails, Ruby and Sass

Notable Features:

    Code snippets, autocomplete and automatic refactoring
    Project tree allows for quick code analysis
    Rails Models Diagram
    Rails Project View
    RubyMotion allows for iOS development
    Stack support includes Bundler, pik, rbenv, RVM and more
    JavaScript, CoffeeScript and Ruby debuggers
    Integration with CVS, Git, Mercurial, Perforce and Subversion
    Bundled keyboard schemes

Drawbacks: Your machine needs at least 4GB of RAM for RubyMine to run smoothly. Some users also complain about the lack of GUI customization options.
10. Xcode
#

Xcode IDE is free, open source, and part of Xcode, which is a collection of tools for making apps for Apple devices such as the iPad, iPhone and Mac. Integration with Cocoa Touch makes development in the Apple environment a breeze, and you can enable services such as Game Center or Passbook with a single mouse click. Built-in communication with the developer’s website helps users produce fully functioning apps on the fly.

Languages Supported: AppleScript, C, C++, Java, Objective-C

Notable Features:

    UI controls can be easily connected with implementation code
    Apple LLVM compiler scans code offers advice for addressing performance issues
    Assistant function allows for split-code workspace
    Jump bar permits quick navigation
    Interface Builder lets user build prototypes without writing any code
    UI and source code can be graphically connected to sketch complex interface prototypes in just minutes
    Version Editor includes log files and commit timeline
    Branch and merge for distributed teams
    Test Navigator lets you quickly test applications at any point during development
    Automatically builds, analyzes, tests, and archives projects thanks to Integration with OX X server
    Workflow is highly customizable with Tabs, Behaviors, and Snippets
    Instrument library and asset catalog

Drawbacks: You obviously need an Apple machine to run Xcode, and keep in mind that you need a developers license to upload apps to the Apple Store.