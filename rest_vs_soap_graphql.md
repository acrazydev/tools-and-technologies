
- SOAP :- 
    It operates with the two basic functions – GET and POST.
    GET is used to retrieve data from the server, while POST is used to add or modify data.
    The main idea behind designing SOAP was to ensure that programs built on different platforms
    and programming languages could exchange data in an easy manner.

When to use SOAP api :-
    SOAP is good for applications that require formal contracts between the API and consumer
    since it can enforce the use of formal contracts by using WSDL (Web Services Description
    Language). Additionally, SOAP has built in WS-Reliable messaging to increase security in
    asynchronous execution and processing. Finally, SOAP has built-in stateful operations.
    REST is naturally stateless, but SOAP is designed to support conversational state management.

- REST :-
    This was designed specifically for working with components such as media components, 
    files or even objects on a particular hardware device.  A Restful service would use the 
    normal HTTP verbs of GET, POST, PUT and DELETE for working with the required components.

When to usw REST api:-    
    Client-Server: This constraint operates on the concept that the client and the server
    should be separate from each other and allowed to evolve individually.

    Stateless: REST APIs are stateless, meaning that calls can be made independently of one another, 
    and each call contains all of the data necessary to complete itself successfully.        

- GraphQL :-
    GraphQL is an open-source data query and manipulation language for APIs, and a runtime for
    fulfilling queries with existing data.It allows clients to define the structure of the data
    required, and the same structure of the data is returned from the server, therefore preventing
    excessively large amounts of data from being returned.

    When to use GraphQL:
    GraphQL is used to when we want to ftech data from server side and allows client to 
    send query based on their requirement.


- Advantages of using JSON

Here are the important benefits/ pros of using JSON:

1. Provide support for all browsers
2. Easy to read and write
3. Straightforward syntax
4. You can natively parse in JavaScript using eval() function
5. Easy to create and manipulate
6. Supported by all major JavaScript frameworks
7. Supported by most backend technologies
8. JSON is recognized natively by JavaScript
9. It allows you to transmit and serialize structured data using a network connection.
10. You can use it with modern programming languages.
11. JSON is text which can be converted to any object of JavaScript into JSON and send this JSON to the server.

- Advantages of using XML

Here are significant benefits/cons of using XML:

1. Makes documents transportable across systems and applications. With the help of XML, you can exchange data quickly between different platforms.
2. XML separates the data from HTML
3. XML simplifies platform change process

Disadvantages of using JSON

Here are cons/ drawback of using JSON:

1. No namespace support, hence poor extensibility
2. Limited development tools support
3. It offers support for formal grammar definition

- Disadvantages of using XML

Here, are cons/ drawbacks of using XML:

1. XML requires a processing application
2. The XML syntax is very similar to other alternatives 'text-based' data transmission formats which is sometimes confusing
3. No intrinsic data type support
4. The XML syntax is redundant
5. Doesn't allow the user to create his tags.



- KEY DIFFERENCE (JSON and XML)

1. JSON object has a type whereas XML data is typeless.
2. JSON does not provide namespace support while XML provides namespaces support.
3. JSON has no display capabilities whereas XML offers the capability to display data.
4. JSON is less secured whereas XML is more secure compared to JSON.
5. JSON supports only UTF-8 encoding whereas XML supports various encoding formats.

### [HTTP STATUS CODES](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)