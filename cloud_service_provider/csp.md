### 1) Amazon Web Service (AWS) - 
    AWS is the safest and protected platform of cloud service which offers a wide set of infrastructure services like database storage, computing power, networking.
    Using this AWS one can host the static websites.
    By using such services, users are able to build complicated applications that are trustworthy, scalable and flexible.
    One can have the hands-on experience of AWS for free.

### 2) Microsoft Azure - 
    Microsoft Azure is used for deploying, designing and managing the applications through a worldwide network.
    Previously Microsoft Azure was known as Windows Azure.
    This Cloud computing service supports various operating systems, databases, tools, programming languages and frameworks.
    A free trial version of Microsoft Azure is available for 30 days.

### 3) Google Cloud Platform -
    Google Cloud Platform uses resources such as computers, virtual machines, hard disks, etc. located at Google data centers.
    Google Cloud Platform is an integrated storage used by developers and enterprises for live data.
    Apart from the free trial, this service is available at various flexible payment plans based on Pay-As-You-Go (PAYG).

### 4) Adobe - 
    Adobe offers many products that provide cloud services. Few among them are Adobe Creative Cloud, Adobe Experience Cloud, and Adobe Document Cloud.
    Adobe Creative Cloud service is a SaaS, that offers its users to access the tools offered by Adobe like editing the videos, photography, graphic designing.
    Adobe Experience Cloud offers its users to access a broad set of solutions for advertising, building campaigns and gaining intelligence on business.
    Adobe Document Cloud is a complete solution for digital documentation.

### VMWare - 
    VMware is a universal leader in virtualization and Cloud Infrastructure.
    VMware’s cloud computing is exclusive and helps in reducing the IT intricacy, lower the expenses, provides flexible agile services.
    VMware vCloud Air is a safe and protected public cloud platform that offers networking, storage, disaster recovery, and computing.
    VMware’s Cloud solutions facilitate to maximize your organization’s profits of cloud computing by combining the services, technologies, guidance needed to operate and manage the staff.

### IBM cloud -
    IBM Cloud offers Iaas, PaaS, and SaaS through all the available cloud delivery models.
    Using IBM Cloud one can have the freedom to select and unite your desired tools, data models and delivery models in designing/creating your next-generation services or applications.
    IBM Cloud is used to build pioneering way outs that can gain value for your businesses and industry.
    With the IBM Bluemix Cloud platform, one can incorporate highly performing cloud communications and services into your IT environment.