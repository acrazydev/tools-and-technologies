## * Most popular frontend Technologies*

1. Vue.js

Vue.js is a JavaScript library for developing distinct web interfaces. Its core library focuses on the view layer only. Therefore, you can conveniently integrate it with other libraries and tools to achieve desired outputs. Also, it is capable of powering Single Page Applications when you merge it with other tools and libraries.

2. React

    React is developed and maintained by Facebook and used in their own products, including Instagram and WhatsApp. It has been around for around five years now, so it’s not exactly new. It’s also one of the most popular projects on GitHub, with about 119,000 stars at the time of writing. Sounds good to me.

3. Angular

    Angular has been around less then React, but it’s not a new kid on the block. It’s maintained by Google and, as mentioned by Igor Minar, used in more than 600 hundred applications in Google such as Firebase Console, Google Analytics, Google Express, Google Cloud Platform and more.

    Features

    Both frameworks share some key features in common: components, data binding, and platform-agnostic rendering.

    **- Angular**

    1) Dependency injection
    2) Templates, based on an extended version of HTML
    3) Routing, provided by @angular/router
    4) Ajax requests using @angular/common/http
    5) @angular/forms for building forms
    6) Component CSS encapsulation
    7) XSS protection
    8) Utilities for unit-testing components.

Some of these features are built-in into the core of the framework and you don’t have an option not to use them. This requires developers to be familiar with features such as dependency injection to build even a small Angular application. Other features such as the HTTP client or forms are completely optional and can be added on an as-needed basis.
React

With React, you’re starting off with a more minimalistic approach. If we’re looking at just React, here’s what we have:

    No dependency injection
    Instead of classic templates, it has JSX, an XML-like language built on top of JavaScript
    State management using setState and the Context API.
    XSS protection
    Utilities for unit-testing components.

Not much. And this can be a good thing. It means that you have the freedom to choose whatever additional libraries to add based on your needs. The bad thing is that you actually have to make those choices yourself. Some of the popular libraries that are often used together with React are:

    React-router for routing
    Fetch (or axios) for HTTP requests
    A wide variety of techniques for CSS encapsulation
    Enzyme for additional unit-testing utilities.

We’ve found the freedom of choosing your own libraries liberating. This gives us the ability to tailor our stack to particular requirements of each project, and we didn’t find the cost of learning new libraries that high.