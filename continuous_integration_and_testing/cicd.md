### Continuous Integration Tools

- Jenkins - 
    *Jenkins* is an open-source CI tool written in Java. It originated as the fork of Hudson when the Oracle bought the Sun Microsystems. Jenkins is a cross-platform CI tool and it offers configuration both through GUI interface and console commands.

    What makes Jenkins very flexible is the feature extension through plugins. Jenkins plugin list is very comprehensive and you can easily add your own. Besides extensibility, Jenkins prides itself on distributing builds and test loads on multiple machines. It is published under MIT license so it is free to use and distribute.

    Verdict: One of the best solutions out there, both powerful and flexible at the same time. The learning curve could be a bit steep, but if you need flexibility it very well pays off to learn how to use it.

- Travis CI -
    *Travis CI* is one of the oldest hosted solutions out there and it has won the trust of many people. Although it’s mostly known for the hosted solution, it offers the on-premise version too in a form of enterprise package.
    Verdict: A Mature solution that offers both hosted and On-premises variants, loved and used by many teams, very well documented.

### Continuous Testing Tools

- Selenium -
    Selenium is probably the household name when it comes to web automated testing. Selenium is an open-source framework that is a great candidate for a team who lean toward continuous testing adoption.

    Selenium is a go-to choice for Quality Assurance (QA) engineers with an advanced level of programming skill. It requires a deep understanding of how framework work to set up and implementation to your current development cycle. 

    Selenium supports a wide range of popular OSs (Windows, macOS, Linux) and browsers (Chrome, Firefox, Safari) for cross-environment testing.

- IBM Rational Functional Tester - 
    IBM's Rational Functional Tester (RFT) supports a variety of applications and allows for both storyboard testing and test scripting. It includes API testing, functional UI testing, and performance testing. It enables shift left testing to run these tests much earlier in the development cycle, allowing better collaboration between testers and developers to identify and fix bugs much earlier in the development cycle.