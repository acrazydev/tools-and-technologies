## *Most popular backend frameworks -*

- ### Express -
> Express.js framework is a lightweight and flexible Node.js application framework, that provides a wide range of functions for building single-page, multi-page and hybrid web applications. This framework makes the creation of websites and apps using Node.js very simple.
> - Pros
> > * ExpressJS developers can appreciate versatile middleware modules for performing additional tasks on response and request.
> > *  Integration with miscellaneous template engines including EJS, Vash, Jade and others is available.
> > * You can define an error processing middleware.
> > * Resources and static files of your app are easy to serve.
> > * Creation of REST API server is one of the most attractive features of app development on ExpressJS.
> > * You can take advantage of connection with such databases as MySQL, Redis, and the abovementioned MongoDB.
> 
> - Cons
> > * Performance bottlenecks with heavy computation tasks
> > * Callback hell issue


- ### Django -
> Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of Web development, so you can focus on writing your app without needing to reinvent the wheel. It’s free and open source.

- Pros

    1) Well-established software with many plugins
    2) Admin area out of the box makes dev and production of CMS very easy
    3) ORM support
    4) Clear and defined MVC organization
    5) Highly customizable
    6) Forms framework
    7) Simple database management

- Cons

    1) Feels like too much software for small projects
    2) Template errors fail silently by default
    3) A process only handles a single request at a time
    4) Deep learning curve
    5) Overwhelming features

- ### Flask -
>Flask is considered more “Pythonic” than Django is simply because Flask web application code is, in most cases, more explicit. Flask is the choice of most beginners due to the lack of roadblocks to getting a simple app up and running.

 - Pros

    1) Extremely flexible
    2) Minimalist without sacrificing power
    3) Simple to learn and use
    4) Routing URLs is easy
    5) Small core and easily extensible

 - Cons

    1) Not async-friendly
    2) Limited support and documentation
    3) Lack of database/ORM/forms
    4) Truly limited in features

- ### Rails -
Ruby on Rails, also known as RoR and Rails is a web framework that is one of the most popular tools in web development.
Ruby on Rails was built on the basis of the Ruby programming language which was originally meant to be a technology for fast software development.

 - Advantages -
    1) Tooling — Rails provides fantastic tooling that helps you to deliver more features in less time. It provides a standard structure for web apps, where all the common patterns are taken care of for you.
    2) Libraries — There’s a gem (3rd party module) for just about anything you can think of.
    3) Code Quality — Generally, we find the quality of third-party Ruby code to be significantly higher than their PHP or NodeJS equivalents.
    4) Test Automation — The Ruby community is big into testing and test automation. We believe this is incredibly valuable in helping to deliver good quality software and is one of the reasons the Ruby libraries are so great.
    5) Large Community — Pretty much every major city in the world has a Ruby community that runs regular meetups. It’s one of the most popular languages on social coding site Github.
    6) Productivity — Ruby is an eloquent and succinct language, which when combined with the plethora of 3rd party libraries, enables you to development features incredibly fast. I would say it’s the most productive programming language around.
    7) Next Generation — Ruby on Rails seems to be the language of choice for a number of the popular online code schools, such as Makers Academy, Steer and CodeCademy. This should mean an increase in talented programmers joining the Ruby community over the coming years.

 - Disadvantages of Ruby on Rails?

    1) Runtime Speed — The most cited argument against Ruby on Rails is that it’s “slow”. We would agree, certainly when compared to the runtime speed of NodeJS or GoLang. Though in reality, the performance of a Ruby application is incredibly unlikely to be a bottleneck for a business. In 99% of cases, the bottleneck is going to be elsewhere, such as within the engineering team, IO, database or server architecture, etc. When you get to a significant enough scale to have to worry about Rails runtime speed, then you’re likely to have an incredibly successful application (think Twitter volume) and will have many scaling issues to deal with.
    2) Boot Speed — The main frustration we hear from developers working in Rails is the boot speed of the Rails framework. Depending on the number of gem dependencies and files, it can take a significant amount of time to start, which can hinder developer performance. In recent versions of Rails this has been somewhat combatted by the introduction of Spring, but we feel this could still be faster.
    3) Documentation — It can be hard to find good documentation. Particularly for the less popular gems and for libraries which make heavy use of mixins (which is most of Rails). You’ll often end up finding the test suite acts as documentation and you’ll rely on this to understand behavior. This isn’t itself a bad thing, as the test suite should be the most up-to-date representation of the system, however, it can still be frustrating having to dive into code when sometimes written documentation would have been much quicker.
    4) Multi-Threading — Rails supports multi-threading, though some of the IO libraries do not, as they keep hold of the GIL (Global Interpreter Lock). This means if you’re not careful, requests will get queued up behind the active request and can introduce performance issues. In practice, this isn’t too much of a problem as, if you use a library that relies on GLI, you can switch to multi-process setup. The knock-on effect of this is your application ends up consuming more compute resources than necessary, which can increase your infrastructure costs.

- ### Spring -
    Spring is a framework used for web development in java. It can be thought of as a framework of frameworks because it provides support to various frameworks such as Struts, Hibernate, Tapestry, EJB, JSF etc. The framework, in broader sense, can be defined as a structure where we find solution of the various technical problems.

 Advantages -

    1) Predefined Templates - 
    Spring framework provides templates for JDBC, Hibernate, JPA etc. technologies. So there is no need to write too much code. It hides the basic steps of these technologies.

    2) Loose Coupling - 
    The Spring applications are loosely coupled because of dependency injection.

    3) Easy to test - 
    The Dependency Injection makes easier to test the application. The EJB or Struts application require server to run the application but Spring framework doesn't require server.

    5) Fast Development - 
    The Dependency Injection feature of Spring Framework and it support to various frameworks makes the easy development of JavaEE application.

Disadvantages - 

    1) Complex: One of the major problem faced by the Spring framework is that it is complex! No so clear focus, more than 3000 classes, 49 other tools and tons of the other things make it complicated for the developers.
    2) Longer Configuration: If you are a fresher developer, it would be quite difficult for you to learn Spring framework. The main reason behind this is a whole host of new programming methods and detailing require understanding how to set up the Spring XML configuration file.
    3) Require a lot of XML: If you have ever worked with Spring framework, you might be knowing that applications developed using Spring framework often require a huge amount of XML. So, if you are considering Spring for your next development, be prepared to spare a lot of time coding in XML.
    4) Parallel mechanisms frustrate developers: Parallel mechanisms are useful to perform the same task in different ways. However, when we talk about Spring framework, you will find multiple parallel mechanisms, which at the end confuses developers. It makes developers to spend lots of understanding each of them and choose the best one among them.
    5) Lack of Guidelines: No clear guidance on cross-site scripting attacks and cross-site request attacks in Spring MVC documentation. Also, it suffers from a several security holes.
